/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package aimsproject;

/**
 *
 * @author Admin
 */
public class Aims {
    public static void main(String[] args){
        Order anOrder = new Order();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The lion king");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLenght(87);
        anOrder.addDVD(dvd1);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star war");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George lucas");
        dvd2.setLenght(124);
        anOrder.addDVD(dvd2);  
        
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Alladin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLenght(90);
        anOrder.addDVD(dvd3);
        
        System.out.print("Total Cost is : ");
        System.out.println(anOrder.totalCost());
    }
}
