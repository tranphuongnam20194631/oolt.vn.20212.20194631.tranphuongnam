/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package aimsproject;
import java.awt.datatransfer.DataFlavor;
import java.util.Scanner;
/**
 *
 * @author Admin
 */
public class MyDate {
    private int day;
    private int month;
    private int year;

    static int checkMonth(String strMonth) {
        if (strMonth.equals("January") || strMonth.equals("Jan.")
                || strMonth.equals("Jan") || strMonth.equals("1")) {
            return 1;
        }
        if (strMonth.equals("February") || strMonth.equals("Feb.")
                || strMonth.equals("Feb") || strMonth.equals("2")) {
            return 2;
        }
        if (strMonth.equals("March") || strMonth.equals("Mar.")
                || strMonth.equals("Mar") || strMonth.equals("3")) {
            return 3;
        }
        if (strMonth.equals("April") || strMonth.equals("Apr.")
                || strMonth.equals("Apr") || strMonth.equals("4")) {
            return 4;
        }
        if (strMonth.equals("May") || strMonth.equals("5")) {
            return 5;
        }
        if (strMonth.equals("June") || strMonth.equals("Jun")
                || strMonth.equals("6")) {
            return 6;
        }
        if (strMonth.equals("July") || strMonth.equals("Jul")
                || strMonth.equals("7")) {
            return 7;
        }
        if (strMonth.equals("August") || strMonth.equals("Aug.")
                || strMonth.equals("Aug") || strMonth.equals("8")) {
            return 8;
        }
        if (strMonth.equals("September") || strMonth.equals("Sept.")
                || strMonth.equals("Sep") || strMonth.equals("9")) {
            return 9;
        }
        if (strMonth.equals("October") || strMonth.equals("Oct.")
                || strMonth.equals("Oct") || strMonth.equals("10")) {
            return 10;
        }
        if (strMonth.equals("November") || strMonth.equals("Nov.")
                || strMonth.equals("Nov") || strMonth.equals("11")) {
            return 11;
        }
        if (strMonth.equals("December") || strMonth.equals("Dec.")
                || strMonth.equals("Dec") || strMonth.equals("12")) {
            return 12;
        }
        return 0;
    }

    public MyDate() {
        this.day = java.time.LocalDate.now().getDayOfMonth();
        this.month = java.time.LocalDate.now().getMonthValue();
        this.year = java.time.LocalDate.now().getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date) {
        String[] parts = date.split(" ");
        this.month = checkMonth(parts[0]);
        this.year = Integer.parseInt(parts[2]);
        String a = "";
        for (int i = 0; i < parts[1].length(); i++) {
            if (parts[1].charAt(i) >= '0' && parts[1].charAt(i) <= '9') {
                a += parts[1].charAt(i);
            }
        }
        this.day = Integer.parseInt(a);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day >= 1 && day <= 31) {
            this.day = day;
        } else {
            System.out.println("Day value is not correct!");
        }
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month >= 1 && month <= 12) {
            this.month = month;
        } else {
            System.out.println("Month value is not correct!");
        }

    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void accept() {
        String date;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhập vào chuỗi bất kỳ: ");
        date = scanner.nextLine();
        String[] parts = date.split(" ");
        this.month = checkMonth(parts[0]);
        this.year = Integer.parseInt(parts[2]);
        String a = "";
        for (int i = 0; i < parts[1].length(); i++) {
            if (parts[1].charAt(i) >= '0' && parts[1].charAt(i) <= '9') {
                a += parts[1].charAt(i);
            }
        }
        this.day = Integer.parseInt(a);
    }
    
    public void print(){
        System.out.println(month + " " + day + " " + year);
    }
    
    public static void main(String[] args) {
        MyDate aDate1 = new MyDate();
        MyDate aDate2 = new MyDate(22, 3, 2012);
        MyDate aDate3 = new MyDate("Feb 13 2017");
        aDate1.print();
        aDate2.print();
        aDate3.print();
        System.out.println("Day   : " + aDate2.getDay());
        System.out.println("Month : " + aDate2.getMonth());
        System.out.println("Year  : " + aDate2.getYear());
        aDate3.setDay(20);
        aDate3.setMonth(5);
        aDate3.setYear(2022);
        aDate3.print();
        MyDate aDate4 = new MyDate();
        aDate4.accept();
        aDate4.print();
    }

}
