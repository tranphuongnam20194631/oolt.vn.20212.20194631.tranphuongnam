/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package aimsproject;

/**
 *
 * @author Admin
 */
public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    private int qtyOrdered = 0;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
    
    public void addDVD(DigitalVideoDisc d){
        if(qtyOrdered == MAX_NUMBER_ORDERED){
            System.out.println("Order is full");
        }
        else {
            itemsOrdered[qtyOrdered] = d;
            qtyOrdered ++ ;
        }
    }
    public void removeDVD(DigitalVideoDisc d){
        for(int i=0 ; i< MAX_NUMBER_ORDERED; i++){
            if(itemsOrdered[i] == d){
                qtyOrdered--;
                while(i != MAX_NUMBER_ORDERED -1 ){
                    itemsOrdered[i] = itemsOrdered[i+1];
                    i++;
                }
            }
        }
    }
    public float totalCost(){
        float sum =0;
        for(int i=0 ;i< qtyOrdered; i++){
            sum = sum + itemsOrdered[i].getCost();
        }
        return sum;
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    public String name(){
 
        return itemsOrdered[2].getTitle();
        
    }
}
