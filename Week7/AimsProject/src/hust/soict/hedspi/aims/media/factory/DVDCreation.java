package hust.soict.hedspi.aims.media.factory;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import java.util.Scanner;

public class DVDCreation implements MediaCreation{

    @Override
    public Media createMediaFromConsole() {
        Scanner sc = new Scanner(System.in);
        System.out.println("DVD Title: ");
        String title = sc.nextLine();
        System.out.println("DVD Category: ");
        String category = sc.nextLine();
        System.out.println("DVD Cost: ");
        float cost = Float.parseFloat(sc.nextLine());
        System.out.println("DVD Director: ");
        String director = sc.nextLine();
        System.out.println("DVD Length: ");
        int length = Integer.parseInt(sc.nextLine());
        return new DigitalVideoDisc(title, category, cost, director, length);
    }
    
}
