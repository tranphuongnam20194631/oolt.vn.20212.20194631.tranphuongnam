package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public abstract class Media {
    private String title;
    private String category;
    private float cost;

    public Media() {
        super();
    }

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String category) {
        this.title = title;
        this.category = category;
    }
    
    public Media(String title, String category, float cost) {
        super();
        this.title = title;
        this.category = category;
        this.cost = cost;
    }
      
    public abstract Media CreateMediaItem();

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }
    
    @Override
    public String toString(){
        return "Title " + this.title + " - Category " + this.category + " - Cost " + this.cost;
    }
}
