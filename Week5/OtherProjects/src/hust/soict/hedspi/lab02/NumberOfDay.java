import java.util.Scanner;

public class NumberOfDay {

    static int checkMonth(String strMonth){
        if(strMonth.equals("January") || strMonth.equals("Jan.") 
                || strMonth.equals("Jan") || strMonth.equals("1")) return 1;
        if(strMonth.equals("February") || strMonth.equals("Feb.") 
                || strMonth.equals("Feb") || strMonth.equals("2")) return 2;
        if(strMonth.equals("March") || strMonth.equals("Mar.") 
                || strMonth.equals("Mar") || strMonth.equals("3")) return 3;
        if(strMonth.equals("April") || strMonth.equals("Apr.") 
                || strMonth.equals("Apr") || strMonth.equals("4")) return 4;
        if(strMonth.equals("May") || strMonth.equals("5")) return 5;
        if(strMonth.equals("June") || strMonth.equals("Jun") 
                || strMonth.equals("6")) return 6;
        if(strMonth.equals("July") || strMonth.equals("Jul") 
                || strMonth.equals("7")) return 7;
        if(strMonth.equals("August") || strMonth.equals("Aug.") 
                || strMonth.equals("Aug") || strMonth.equals("8")) return 8;
        if(strMonth.equals("September") || strMonth.equals("Sept.") 
                || strMonth.equals("Sep") || strMonth.equals("9")) return 9;
        if(strMonth.equals("October") || strMonth.equals("Oct.") 
                || strMonth.equals("Oct") || strMonth.equals("10")) return 10;
        if(strMonth.equals("November") || strMonth.equals("Nov.") 
                || strMonth.equals("Nov") || strMonth.equals("11")) return 11;
        if(strMonth.equals("December") || strMonth.equals("Dec.") 
                || strMonth.equals("Dec") || strMonth.equals("12")) return 12;
        return 0;        
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        int month;
        do {            
            System.out.print("Enter month: ");
            String strMonth = keyboard.nextLine();
            month = checkMonth(strMonth);
        } while (month == 0);
        
        System.out.print("Enter year: ");
        int year = keyboard.nextInt();
        if (month == 1 || month == 3 || month == 5 || month == 7 
                || month == 8 || month == 10 || month == 12) {
            System.out.println(month + " " + year + " has 31 days.");
        }else if(month == 4 || month == 6 || month == 9 || month == 11){
            System.out.println(month + " " + year + " has 30 days.");
        }else if(year % 4 == 0 || (year % 100 != 0 && year % 400 == 0)){
            System.out.println(month + " " + year + " has 29 days.");
        }else{
            System.out.println(month + " " + year + " has 28 days.");
        }
    }
}
