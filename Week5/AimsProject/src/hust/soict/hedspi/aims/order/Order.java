package hust.soict.hedspi.aims.order;

import hust.soict.hedspi.aims.utils.MyDate;
import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import java.util.Scanner;

public class Order {

    public static final int MAXIMUM_ORDERED_ITEMS = 10;
    public static final int MAXIMUM_ORDERS = 5;

    private static int nbOrders = 0;
    private MyDate dateOrdered;

    private int qtyDVDOrdered = 0;
    private int qtyCDOrdered = 0;

    private int nbOrderedItems;
    private DigitalVideoDisc[] itemsDVDOrdered = new DigitalVideoDisc[MAXIMUM_ORDERED_ITEMS];
    private CompactDisc[] itemsCDOrdered = new CompactDisc[MAXIMUM_ORDERED_ITEMS];

    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(MyDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    private Order() {
        dateOrdered = new MyDate();
    }

    public static Order createOrder() {
        if (nbOrders >= MAXIMUM_ORDERS) {
            System.out.println("You have reached maximum orders");
            return null;
        } else {
            nbOrders++;
            return new Order();
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyDVDOrdered < MAXIMUM_ORDERS) {
            itemsDVDOrdered[qtyDVDOrdered] = disc;
            qtyDVDOrdered++;
            System.out.println("The DVD disc has been added");
        } else {
            System.out.println("The order is almost full");
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
        addDigitalVideoDisc(dvd1);
        addDigitalVideoDisc(dvd2);
    }

    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
        int i = 0;
        while (dvdList[i] != null) {
            addDigitalVideoDisc(dvdList[i]);
            i++;
        }
    }

    public void addCompactDisc(CompactDisc disc) {
        if (qtyCDOrdered < MAXIMUM_ORDERS) {
            itemsCDOrdered[qtyCDOrdered] = disc;
            qtyCDOrdered++;
            System.out.println("The CD disc has been added");
        } else {
            System.out.println("The order is almost full");
        }
    }

    public void addCompactDisc(CompactDisc cd1, CompactDisc cd2) {
        addCompactDisc(cd1);
        addCompactDisc(cd2);
    }

    public void addCompactDisc(CompactDisc[] cdList) {
        int i = 0;
        while (cdList[i] != null) {
            addCompactDisc(cdList[i]);
            i++;
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        for (int i = 0; i <= MAXIMUM_ORDERS; i++) {
            if (itemsDVDOrdered[i] == disc) {
                qtyDVDOrdered--;
                for (int j = i; j < MAXIMUM_ORDERS - 1; j++) {
                    itemsDVDOrdered[j] = itemsDVDOrdered[j + 1];
                }
                System.out.println("The DVD disc has been remove");
                break;
            }
        }
    }

    public void removeCompactDisc(CompactDisc disc) {
        for (int i = 0; i <= MAXIMUM_ORDERS; i++) {
            if (itemsCDOrdered[i] == disc) {
                qtyCDOrdered--;
                for (int j = i; j < MAXIMUM_ORDERS - 1; j++) {
                    itemsCDOrdered[j] = itemsCDOrdered[j + 1];
                }
                System.out.println("The CD disc has been remove");
                break;
            }
        }
    }
    
    public float totalCost() {
        float sum = 0;
        for (int i = 0; i < qtyDVDOrdered; i++) {
            sum += itemsDVDOrdered[i].getCost();
        }
        for (int i = 0; i < qtyCDOrdered; i++) {
            sum += itemsCDOrdered[i].getPrice();
        }
        return sum;
    }

    public float totalCost(DigitalVideoDisc dvd) {
        float sum = 0;
        for (int i = 0; i < qtyDVDOrdered; i++) {
            sum += itemsDVDOrdered[i].getCost();
        }
        for (int i = 0; i < qtyCDOrdered; i++) {
            sum += itemsCDOrdered[i].getPrice();
        }
        sum -= dvd.getCost();
        return sum;
    }
    
    public void printOrder() {
        System.out.println("************************************Order********************************");
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered Item: ");
        int i = 0;
        while (itemsDVDOrdered[i] != null) {
            System.out.print((i + 1) + ". ");
            System.out.println(itemsDVDOrdered[i]);
            i++;
        }
        int j = 0;
        while (itemsCDOrdered[j] != null) {
            System.out.print((i + 1) + ". ");
            itemsCDOrdered[j].toStringCD();
            i++;
            j++;
        }
        System.out.println("Specifying a lucky and free item");
        DigitalVideoDisc dvd = this.getALuckyItem();
        System.out.println(dvd);
        System.out.print("Total Cost: ");
        System.out.println(totalCost() - dvd.getCost());
        System.out.println("****************************************************************************");
    }

    public DigitalVideoDisc getALuckyItem() {
        int ramdomInt = (int) (Math.random() * qtyDVDOrdered);
        return itemsDVDOrdered[ramdomInt];

    }
}
