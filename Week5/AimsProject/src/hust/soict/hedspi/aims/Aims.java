package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;

public class Aims {

    public static void main(String[] args) {
        Order o1 = Order.createOrder();
        Order o2 = Order.createOrder();
        Order o3 = Order.createOrder();
        Order o4 = Order.createOrder();
        Order o5 = Order.createOrder();
        Order o6 = Order.createOrder();
        System.out.println(o6);
        DigitalVideoDisc dvdList[] = new DigitalVideoDisc[10];
        CompactDisc cdList[] = new CompactDisc[10];
        
        dvdList[0] = new DigitalVideoDisc("The Lion King");
        dvdList[0].setCategory("Animation");
        dvdList[0].setCost(19.95f);
        dvdList[0].setDirector("Roger Allers");
        dvdList[0].setLength(87);
        
        dvdList[1] = new DigitalVideoDisc("Star Wars");
        dvdList[1].setCategory("Science Fiction");
        dvdList[1].setCost(24.95f);
        dvdList[1].setDirector("George Lucas");
        dvdList[1].setLength(124);
        
        dvdList[2] = new DigitalVideoDisc("Aladdin");
        dvdList[2].setCategory("Animation");
        dvdList[2].setCost(18.99f);
        dvdList[2].setDirector("John Musker");
        dvdList[2].setLength(90);
        
        //o1.addDigitalVideoDisc(dvdList[0], dvdList[2]);
        o1.addDigitalVideoDisc(dvdList);
                
        cdList[0] = new CompactDisc("5cm/s");
        cdList[0].setCategory("Animetion");
        cdList[0].setArtist("dfhdfh");
        cdList[0].setDirector("kim");
        cdList[0].setPrice(10.3f);
        
        cdList[1] = new CompactDisc("Konosuba");
        cdList[1].setCategory("Animetion");
        cdList[1].setArtist("alo");
        cdList[1].setDirector("tien");
        cdList[1].setPrice(13.34f);
        
        //o1.addCompactDisc(cdList[0], cdList[1]);
        o1.addCompactDisc(cdList);
        
        //o1.removeCompactDisc(cdList[0]);
        //o1.removeDigitalVideoDisc(dvdList[1]);
        
        o1.printOrder();
        
        
        String str1 = new String("Hello");
        String str2 = str1;
        str1 = str1+ " World";
        
        System.out.println("String testing: ");
        System.out.println(str1);
        System.out.println(str2);
        
        System.out.println("DVD object testing");
        DigitalVideoDisc d1= new DigitalVideoDisc("Jungle");
        DigitalVideoDisc d2 = d1;
        d1.setTitle(d1.getTitle()+" Legends");
        System.out.println(d1.getTitle());
        System.out.println(d2.getTitle());
    } 
}
