package aimsproject;

public class CompactDisc {
    private String title;
    private String category;
    private String artist;
    private String director;
    private float price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public CompactDisc(String title) {
        this.title = title;
    }

    public CompactDisc(String title, String category) {
        this.title = title;
        this.category = category;
    }

    public CompactDisc(String title, String category, String director) {
        this.title = title;
        this.category = category;
        this.director = director;
    }

    public CompactDisc(String title, String category, String artist, String director, float price) {
        this.title = title;
        this.category = category;
        this.artist = artist;
        this.director = director;
        this.price = price;
    }

    public CompactDisc() {
        super();
    }
    
    public void toStringCD(){
        System.out.println("CD - " + title + " - " + category + " - " + artist + " - " + director + ": " + price + "$");
    }
    
    
}
