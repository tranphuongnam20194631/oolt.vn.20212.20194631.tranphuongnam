/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javabasics;
import java.util.Scanner;
/**
 *
 * @author Admin
 */
public class InputFromKeyboard {
    public static void main(String args[]){
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("What 's your name ?");
        String strName =keyboard.nextLine();
        System.out.println("how old are you ?");
        int iAge= keyboard.nextInt();
        System.out.println("how tall are you (m)?");
        double dHeight = keyboard.nextDouble();
        
        
        System.out.println("Mrs/Ms. " + strName + " ," + iAge + " years old. " + "Your height is " + dHeight + ".");
    }
}
