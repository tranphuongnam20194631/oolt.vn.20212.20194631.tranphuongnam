/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javabasics;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class SortAndAverage {
    public static void SortArray(int[] A){
        for(int i=0;i< A.length -1;i++){
            int temp = A[i];
            for(int j = i + 1; j<A.length ; j++){
                if(temp > A[j]){
                    A[i] = A[j];
                    A[j] = temp;
                    temp = A[i];
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] Array = new int[5];

        Scanner scan = new Scanner(System.in);
        System.out.println(" vui long nhap gia tri cua mang");
        for (int i = 0; i < Array.length; i++) {
            Array[i] = scan.nextInt();
        }
        SortArray(Array);
        System.out.println(" mang duoc nhap vao la ");
        for (int x : Array) {
            System.out.println(x);
        }
        int sum =0;
        for(int i =0;i< Array.length ; i++){
            sum = sum + Array[i];
        }
        int avg = sum/Array.length;
        System.out.println(" sum of = " + sum + ". Avg of array = " + avg + ".");
    }
}
